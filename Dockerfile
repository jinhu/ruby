FROM registry.gitlab.com/jinhu/alpine:3.5

#copied from cyberdojo
RUN apk --update --no-cache add \
    openssl ca-certificates \
    ruby ruby-io-console ruby-dev ruby-irb ruby-bundler ruby-bigdecimal \
    bash tzdata

#what does this line do?
#RUN adduser -D -H -u 19661 jinhu

CMD [irb]